package com.example.quicksnack_mobile.classes;

import android.content.Context;
import android.widget.ProgressBar;

import com.example.quicksnack_mobile.request.Api;
import com.example.quicksnack_mobile.request.RequestLogin;

import java.util.HashMap;

public class Login {

    private String login;
    private String senha;

    public Login(String login, String senha){

        this.login = login;
        this.senha = senha;

    }

    public String getLogin() {
        return login;
    }

    public String getSenha() {
        return senha;
    }


    public void loginUser(ProgressBar progressBar, Context context) throws InterruptedException {




        HashMap<String, String> params = new HashMap<>();
        params.put("login", this.login);
        params.put("senha",this.senha);


       RequestLogin request = new  RequestLogin(Api.URL_LOGIN, params, Api.CODE_POST_REQUEST,
               progressBar, context);

        request.execute();




    }

}
