package com.example.quicksnack_mobile.classes;

import java.util.Date;

public class Estoque {

    private String produto;
    private int quantidade;
    private String data_cadastro;


    public Estoque(){}



    public Estoque(String prod, int quantidade, String data){
        this.produto = prod;
        this.quantidade = quantidade;
        this.data_cadastro = data;
    }

    public String getProduto() {
        return produto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public String getData_cadastro() {
        return data_cadastro;
    }




}
