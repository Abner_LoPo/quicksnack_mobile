package com.example.quicksnack_mobile.classes;

public class Produtos {

    private String descricao;
    private String preco;
    private String desconto;

    public Produtos(){}


    public Produtos(String descricao, String preco, String desc ){

        this.descricao = descricao;
        this.preco = preco;
        this.desconto = desc;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getPreco() {
        return preco;
    }


    public String getDesconto() {
        return desconto;
    }



}
