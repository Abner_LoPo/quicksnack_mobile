package com.example.quicksnack_mobile;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private TextView navUsername,  navUsernameCargo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final DrawerLayout drawerLayout = findViewById(R.id.drawerLayout);
        findViewById(R.id.imageMenu).setOnClickListener((view) -> {

            drawerLayout.openDrawer(GravityCompat.START);


        });




        NavigationView navigationView = findViewById(R.id.navigationView);
        navigationView.setItemIconTintList(null);


        View headerView = navigationView.getHeaderView(0);
        navUsername = (TextView) headerView.findViewById(R.id.usernamelogin);
        navUsernameCargo = (TextView) headerView.findViewById(R.id.usernamecargo);
        Intent intent = getIntent();
        String[] dadosLogin = intent.getStringArrayExtra(ActivityLogin.EXTRA_MESSAGE);

        navUsername.setText(dadosLogin[1]);
        navUsernameCargo.setText(dadosLogin[2]);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Logout");
        builder.setMessage("Deseja sair do login:  "+navUsername.getText()+"?");

        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Intent intent = new Intent(MainActivity.this, ActivityLogin.class);
                finish();
                startActivity(intent);


                Toast.makeText(getApplicationContext(), "Saindo......", Toast.LENGTH_SHORT).show();


            }
        });

        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getApplicationContext(), "Cancelando......", Toast.LENGTH_SHORT).show();

                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });



        AlertDialog alertDialog = builder.create();



        NavController navController = Navigation.findNavController(this,R.id.navHostFragment);
        NavigationUI.setupWithNavController(navigationView,navController);




        final TextView textTitle = findViewById(R.id.textTitle);
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener(){

            @Override
            public void onDestinationChanged(@NonNull NavController controller,  @NonNull NavDestination destination, @Nullable Bundle arguments) {


                if(!destination.getLabel().toString().equalsIgnoreCase("Logout")) {
                    textTitle.setText(destination.getLabel());
                }else{

                    alertDialog.show();
                }
            }

        });







    }
}