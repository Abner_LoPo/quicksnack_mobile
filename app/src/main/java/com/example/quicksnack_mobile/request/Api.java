package com.example.quicksnack_mobile.request;

public class Api {
    private static final String ROOT_URL= "http://192.168.1.245/QuickSnackMobile_API/v1/Api.php?apicall=";
    public static final int CODE_GET_REQUEST = 1024;
    public static final int CODE_POST_REQUEST = 1025;
    public static final String URL_GET_CATEGOARIAS = ROOT_URL + "getcategorias";
    public static final String URL_GET_MESAS = ROOT_URL +  "getmesas";
    public static final String URL_GET_PRODUTOCAT = ROOT_URL + "getProdutos&categoria=";
    public static final String URL_GET_ESTOQUEDESC = ROOT_URL + "getEstoque&descricao=";
    public static  final String URL_UPDATE_STATUSM = ROOT_URL + "alterarMesa";
    public static  final String URL_LOGIN= ROOT_URL + "login";
}
