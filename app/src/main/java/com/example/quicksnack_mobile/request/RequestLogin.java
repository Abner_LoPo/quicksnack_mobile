package com.example.quicksnack_mobile.request;


import static android.view.View.GONE;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.quicksnack_mobile.ActivityLogin;
import com.example.quicksnack_mobile.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


//Classe para realizar solicitação de rede

public class RequestLogin extends AsyncTask<Void, Void, String> {



    String url;//-->  o url para onde precisamos enviar a solicitação
    HashMap<String, String> params;//--> os parametros
    int requestCode;//--> o código da solicitação para definir se é um GET ou POST
    ProgressBar progressBar;
    Context context;//--> Obter o contexto do aplicativo
    boolean resultRequest;




    // construtor para inicializar valores
    public RequestLogin(String url, HashMap<String, String> params, int requestCode, ProgressBar progressBar, Context contexto) {
        this.url = url;
        this.params = params;
        this.requestCode = requestCode;
        this.progressBar = progressBar;
        this.context = contexto;
    }


    public boolean getResultRequest(){
        return this.resultRequest;
    }

    //quando a tarefa começou, exibir uma barra de progresso
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressBar.setVisibility(View.VISIBLE);
    }


    // este método dará a resposta da solicitação
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        progressBar.setVisibility(GONE);
        try {
            JSONObject object = new JSONObject(s);
            if (!object.getBoolean("error")) {

                List<String> login = new ArrayList<>();

               String[] atr = new String[3];
               atr[1] =  object.getJSONObject("login").getString("login");
               atr[2] = object.getJSONObject("login").getString("cargo");

               login.toArray(atr);


                Intent intent = new Intent(this.context, MainActivity.class);
                intent.setFlags(intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(ActivityLogin.EXTRA_MESSAGE,atr);
                this.context.startActivity(intent);


            }else {

                Toast.makeText(this.context, object.getString("message"),
                        Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    // a operação da rede será realizada em segundo plano
    @Override
    protected String doInBackground(Void... voids) {
        RequestHandler requestHandler = new RequestHandler();

        if (requestCode == Api.CODE_POST_REQUEST)
            return requestHandler.sendPostRequest(url, params);


        if (requestCode == Api.CODE_GET_REQUEST)
            return requestHandler.sendGetRequest(url);

        return null;
    }




}