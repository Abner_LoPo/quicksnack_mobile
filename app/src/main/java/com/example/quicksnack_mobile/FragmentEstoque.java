package com.example.quicksnack_mobile;

import static android.view.View.GONE;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.quicksnack_mobile.classes.Estoque;
import com.example.quicksnack_mobile.classes.Mesa;
import com.example.quicksnack_mobile.request.Api;
import com.example.quicksnack_mobile.request.RequestHandler;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class FragmentEstoque extends Fragment {


    private TextInputEditText textInputEditPesProd;
    private Context thiscontext;
    private List<Estoque> listProdEtq;
    private TableLayout tableLayoutProdutosEtq;
    private ProgressBar progressBar;
    String descricao;

    public FragmentEstoque() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {

        this.thiscontext = context;
        super.onAttach(context);
    }

    public static FragmentEstoque newInstance(String param1, String param2) {
        FragmentEstoque fragment = new FragmentEstoque();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_estoque, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        //3. Event
        super.onViewCreated(view, savedInstanceState);



        textInputEditPesProd = view.findViewById(R.id.textInputEditPesProd);
        tableLayoutProdutosEtq = view.findViewById(R.id.tableLayoutProdutosEtq);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBarProdEtq);
        listProdEtq = new ArrayList<>();


        textInputEditPesProd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                descricao = textInputEditPesProd.getText().toString();



                buscarProdutoEstoque request = new buscarProdutoEstoque(Api.URL_GET_ESTOQUEDESC+
                        descricao,
                        null,
                        Api.CODE_GET_REQUEST,
                        progressBar,
                        thiscontext);

                request.execute();


            }

            @Override
            public void afterTextChanged(Editable editable) {



            }
        });


        buscarProdutoEstoque request = new buscarProdutoEstoque(Api.URL_GET_ESTOQUEDESC+
                "",
                null,
                Api.CODE_GET_REQUEST,
                progressBar,
                thiscontext);

        request.execute();

   }
    private void atualizaProdutoEtq(List<Estoque> estoque){

        tableLayoutProdutosEtq.removeAllViews();
        tableLayoutProdutosEtq.setStretchAllColumns(true);
        tableLayoutProdutosEtq.bringToFront();

        TableRow trHeader = new TableRow(getContext());

        TextView c1Header = new TextView(thiscontext);
        c1Header.setText("Produto");
        c1Header.setTextColor(Color.BLACK);

        TextView c2Header = new TextView(thiscontext);
        c2Header.setText("Quantidade");
        c2Header.setTextColor(Color.BLACK);


        TextView c3Header = new TextView(thiscontext);
        c3Header.setText("Data Cadastro");
        c3Header.setTextColor(Color.BLACK);


        c1Header.setGravity(Gravity.CENTER);
        c2Header.setGravity(Gravity.CENTER);
        c3Header.setGravity(Gravity.CENTER);

        trHeader.addView( c1Header);
        trHeader.addView( c2Header);
        trHeader.addView( c3Header);

        tableLayoutProdutosEtq.addView(trHeader);


        for (int i = 0; i< estoque.size(); i++){

            TableRow tr = new TableRow(thiscontext);

            TextView c1 = new TextView(thiscontext);
            c1.setText(estoque.get(i).getProduto());

            TextView c2 = new TextView(thiscontext);
            c2.setText(Integer.toString(estoque.get(i).getQuantidade()));

            TextView c3 = new TextView(thiscontext);
            c3.setText(estoque.get(i).getData_cadastro());


            c1.setGravity(Gravity.CENTER);
            c2.setGravity(Gravity.CENTER);
            c3.setGravity(Gravity.CENTER);

            tr.addView(c1);
            tr.addView(c2);
            tr.addView(c3);


            tableLayoutProdutosEtq.addView(tr);

        }

    }


    private void refreshProdutoEtq(@NonNull JSONArray prodEstoque) throws JSONException {
        listProdEtq.clear();

        for (int i = 0; i < prodEstoque.length(); i++) {
            JSONObject obj = prodEstoque.getJSONObject(i);

            listProdEtq.add(new Estoque(obj.getString("descricao"),
                                        obj.getInt("quantidade"),
                                        obj.getString("data_cad")));
        }

        /*[Colocar estoque na Table]*/
        atualizaProdutoEtq(listProdEtq);
    }

    public class buscarProdutoEstoque extends AsyncTask<Void, Void, String> {

        private String url;
        private int requestCode;
        private HashMap<String, String> dados;
        private Context context;
        private ProgressBar progressBar;


        public buscarProdutoEstoque(String url, HashMap<String, String> dados, int requestCode, ProgressBar progressBar,
                            Context contexto) {
            this.url = url;
            this.dados= dados;
            this.requestCode = requestCode;
            this.progressBar = progressBar;
            this.context = contexto;
        }

        //quando a tarefa começou, exibir uma barra de progresso
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }



        // este método dará a resposta da solicitação
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressBar.setVisibility(GONE);
            try {
                JSONObject object = new JSONObject(s);
                if (!object.getBoolean("error")) {




                    refreshProdutoEtq(object.getJSONArray("estoque"));


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        // a operação da rede será realizada em segundo plano
        @Override
        protected String doInBackground(Void... voids) {

            RequestHandler requestHandler = new RequestHandler();
            if (requestCode == Api.CODE_POST_REQUEST)
                return requestHandler.sendPostRequest(url, dados);


            if (requestCode == Api.CODE_GET_REQUEST)
                return requestHandler.sendGetRequest(url);

            return null;


        }


    }
}