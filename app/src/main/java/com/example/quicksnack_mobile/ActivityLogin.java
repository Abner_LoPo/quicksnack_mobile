package com.example.quicksnack_mobile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.quicksnack_mobile.classes.Login;

public class ActivityLogin extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.quicksnack_mobile.MESSAGE";
    private EditText editTextInputEditUsername, editInputEditPassword;
    private Button btnLogin;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextInputEditUsername = findViewById(R.id.textInputEditUsername);
        editInputEditPassword = findViewById(R.id.textInputEditPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String login=  editTextInputEditUsername.getText().toString().trim();
                String password = editInputEditPassword.getText().toString().trim();


                if(verificarInput(login, password)){

                    Login login1 = new Login(login, password);

                    try {
                        login1.loginUser(progressBar, getApplicationContext());

                    }catch (Exception ex){}

                }
            }
        });



     /*   editInputEditPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if(motionEvent.getAction() == MotionEvent.ACTION_UP){

                    if(motionEvent.getRawY() >= (editInputEditPassword.getRight() - editInputEditPassword.getCompoundDrawables()[2].getBounds().width())){

                        Toast.makeText(getApplicationContext(), "Clickou no botão",
                                Toast.LENGTH_SHORT).show();

                            return true;

                    }

                }

                return false;
            }
        });*/


    }

    public boolean verificarInput(String userName, String senha){



        //Validar campos
        if(TextUtils.isEmpty(userName)){
            editTextInputEditUsername.setError("Digite o nome de usuário");
            editInputEditPassword.requestFocus();
            return false;
        }

        if(TextUtils.isEmpty(senha)){
            editTextInputEditUsername.setError("Digite a senha");
            editInputEditPassword.requestFocus();
            return false;
        }

        return true;
    }
}