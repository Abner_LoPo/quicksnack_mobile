package com.example.quicksnack_mobile;

import static android.view.View.GONE;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.quicksnack_mobile.classes.Mesa;
import com.example.quicksnack_mobile.classes.Produtos;
import com.example.quicksnack_mobile.request.Api;
import com.example.quicksnack_mobile.request.RequestHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FragmentProdutos extends Fragment {

    private Spinner spinnerCategoria;
    private TableLayout tableLayoutProdutos;
    private ProgressBar progressBar;
    private List<Produtos> listProd;
    private Context thiscontext;

    public FragmentProdutos() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {

        this.thiscontext = context;
        super.onAttach(context);
    }

    public static FragmentProdutos newInstance(String param1, String param2) {
        FragmentProdutos fragment = new FragmentProdutos();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //1º event
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //2º event
        return inflater.inflate(R.layout.fragment_produtos, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //3. Event


        tableLayoutProdutos = view.findViewById(R.id.tableLayoutProdutos);
        spinnerCategoria = (Spinner) view.findViewById(R.id.spinnerCategorias);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBarProd);
        listProd = new ArrayList<>();






        spinnerCategoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                RequestProduto requestProduto = new RequestProduto(Api.URL_GET_PRODUTOCAT+Integer.toString(i),null, Api.CODE_GET_REQUEST
                        ,progressBar,thiscontext);

                requestProduto.execute();

               /*toast.makeText(thiscontext, Integer.toString(i+1)+" "+spinnerCategoria.getSelectedItem(),
                        Toast.LENGTH_SHORT).show();*/

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {  }
        });



    }


    private void atualizaProdutos(List<Produtos> produtos){

        tableLayoutProdutos.removeAllViews();
        tableLayoutProdutos.setStretchAllColumns(true);
        tableLayoutProdutos.bringToFront();

        TableRow trHeader = new TableRow(getContext());

        TextView c1Header = new TextView(thiscontext);
        c1Header.setText("Descrição");
        c1Header.setTextColor(Color.BLACK);

        TextView c2Header = new TextView(thiscontext);
        c2Header.setText("Preço de Venda");
        c2Header.setTextColor(Color.BLACK);


        TextView c3Header = new TextView(thiscontext);
        c3Header.setText("Desconto");
        c3Header.setTextColor(Color.BLACK);


        c1Header.setGravity(Gravity.CENTER);
        c2Header.setGravity(Gravity.CENTER);
        c3Header.setGravity(Gravity.CENTER);

        trHeader.addView( c1Header);
        trHeader.addView( c2Header);
        trHeader.addView( c3Header);

        tableLayoutProdutos.addView(trHeader);

        for(int i = 0; i < produtos.size(); i++){

            TableRow tr = new TableRow(thiscontext);

            TextView c1 = new TextView(thiscontext);
            c1.setText(produtos.get(i).getDescricao());

            TextView c2 = new TextView(thiscontext);
            c2.setText(produtos.get(i).getPreco());

            TextView c3 = new TextView(thiscontext);
            c3.setText(produtos.get(i).getDesconto());


            c1.setGravity(Gravity.CENTER);
            c2.setGravity(Gravity.CENTER);
            c3.setGravity(Gravity.CENTER);

            tr.addView(c1);
            tr.addView(c2);
            tr.addView(c3);


            tableLayoutProdutos.addView(tr);

        }



    }



    private void refreshProdutosList(@NonNull JSONArray produtos) throws JSONException {
        listProd.clear();

        for (int i = 0; i < produtos.length(); i++) {
            JSONObject obj = produtos.getJSONObject(i);

            listProd.add(new Produtos(
                    obj.getString("descricao"),
                    obj.getString("preco_venda"),
                    obj.getString("max_desconto")));
        }

        /*[ColocarProdutos na Table]*/
        atualizaProdutos(listProd);
    }





    public class RequestProduto extends AsyncTask<Void, Void, String> {

        private String url;
        private int requestCode;
        private HashMap<String, String> dados;
        private Context context;
        private ProgressBar progressBar;


        public  RequestProduto (String url, HashMap<String, String> dados, int requestCode, ProgressBar progressBar,
                            Context contexto) {
            this.url = url;
            this.dados= dados;
            this.requestCode = requestCode;
            this.progressBar = progressBar;
            this.context = contexto;
        }

        //quando a tarefa começou, exibir uma barra de progresso
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }



        // este método dará a resposta da solicitação
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressBar.setVisibility(GONE);
            try {
                JSONObject object = new JSONObject(s);
                if (!object.getBoolean("error")) {

                    Toast.makeText(this.context, object.getString("message"),
                            Toast.LENGTH_SHORT).show();



                    refreshProdutosList(object.getJSONArray("produtos"));


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        // a operação da rede será realizada em segundo plano
        @Override
        protected String doInBackground(Void... voids) {

            RequestHandler requestHandler = new RequestHandler();
            if (requestCode == Api.CODE_POST_REQUEST)
                return requestHandler.sendPostRequest(url, dados);


            if (requestCode == Api.CODE_GET_REQUEST)
                return requestHandler.sendGetRequest(url);

            return null;


        }


    }





}