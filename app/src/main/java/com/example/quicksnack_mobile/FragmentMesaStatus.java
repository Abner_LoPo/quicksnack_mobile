package com.example.quicksnack_mobile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import static android.view.View.GONE;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.quicksnack_mobile.classes.Mesa;
import com.example.quicksnack_mobile.request.Api;
import com.example.quicksnack_mobile.request.RequestHandler;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class FragmentMesaStatus extends Fragment {

    private Context thiscontext;
    private TableRow  parentRow;
    private List<Mesa> mesaList;
    private TableLayout tableLayoutMesas;
    private TextInputEditText textInputEditTextNumberMesa;
    private Spinner spinnerStatusMesa;
    private Button buttonAlterar;
    private ProgressBar progressBarMesa;
    private int  indexF = 0;



    public FragmentMesaStatus() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {

        this.thiscontext = context;
        super.onAttach(context);
    }

    @NonNull
    public  FragmentMesaStatus newInstance(String param1, String param2) {
        FragmentMesaStatus fragment = new FragmentMesaStatus();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

            //1. Event
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //2. Event
        return  inflater.inflate(R.layout.fragment_mesa_status,container,false);

    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        //3. Event
        super.onViewCreated(view, savedInstanceState);

        tableLayoutMesas = (TableLayout) view.findViewById(R.id.tableLayoutMesas);
        textInputEditTextNumberMesa = view.findViewById(R.id.textInputEditTextNumberMesa);
        spinnerStatusMesa = (Spinner) view.findViewById(R.id.spinnerStatusMesa);
        progressBarMesa = (ProgressBar) view.findViewById(R.id.progressBarMesa);
        buttonAlterar = (Button) view.findViewById(R.id.btnMesaStatus);
        mesaList = new ArrayList<>();

        textInputEditTextNumberMesa.setEnabled(false);

        buttonAlterar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String numberMesa = textInputEditTextNumberMesa.getText().toString();
                String status = spinnerStatusMesa.getSelectedItem().toString();

                if(TextUtils.isEmpty(numberMesa)){

                    Toast.makeText(thiscontext, "Selecione uma mesa.",
                            Toast.LENGTH_SHORT).show();
                    return;

                }

                HashMap<String, String> dados = new HashMap<>();
                dados.put("codmesa",numberMesa);
                dados.put("status",status);

                RequestMesa requestMesaAlterar = new RequestMesa(Api.URL_UPDATE_STATUSM, dados, Api.CODE_POST_REQUEST,
                        progressBarMesa,thiscontext);
                requestMesaAlterar.execute();


                limpaCampos();

            }
        });



        RequestMesa requestMesa = new RequestMesa(Api.URL_GET_MESAS, null, Api.CODE_GET_REQUEST,
                progressBarMesa,thiscontext);
        requestMesa.execute();
    }



    private void limpaCampos(){

        textInputEditTextNumberMesa.setText("");
        spinnerStatusMesa.setSelection(0);
        this.indexF = 0;

    }


    private void atualizaMesastatus(List<Mesa> mesas) {

        tableLayoutMesas.removeAllViews();
        tableLayoutMesas.setStretchAllColumns(true);
        tableLayoutMesas.bringToFront();

        TableRow trHeader = new TableRow(getContext());

        TextView c1Header = new TextView(thiscontext);
        c1Header.setText("Nº Mesa");
        c1Header.setTextColor(Color.BLACK);

        TextView c2Header = new TextView(thiscontext);
        c2Header.setText("Situação");
        c2Header.setTextColor(Color.BLACK);


        TextView c3Header = new TextView(thiscontext);
        c3Header.setText("Selecionar");
        c3Header.setTextColor(Color.BLACK);

        c1Header.setGravity(Gravity.CENTER);
        c2Header.setGravity(Gravity.CENTER);
        c3Header.setGravity(Gravity.CENTER);

        trHeader.addView( c1Header);
        trHeader.addView( c2Header);
        trHeader.addView( c3Header);

        tableLayoutMesas.addView(trHeader);


        for(int i = 0; i < mesas.size(); i++){

            TableRow tr = new TableRow(getContext());

            TextView c1 = new TextView(thiscontext);
            c1.setText(String.valueOf(mesas.get(i).getcod_mesa()));

            TextView c2 = new TextView(thiscontext);
            c2.setText(mesas.get(i).getSituacao());

            TextView c6 = new TextView(thiscontext);
            c6.setText("Selecionar");
            c6.setPadding(5,5,5,5);



            c1.setGravity(Gravity.CENTER);
            c2.setGravity(Gravity.CENTER);
            c6.setGravity(Gravity.CENTER);

            tr.addView(c1);
            tr.addView(c2);
            tr.addView(c6);

            final int index = i;
            if(!"Indisponível".equalsIgnoreCase( mesas.get(i).getSituacao())){

                if("Disponível".equalsIgnoreCase( mesas.get(i).getSituacao()))
                c6.setTextColor(Color.parseColor("#C20A10"));

                if("Limpeza".equalsIgnoreCase( mesas.get(i).getSituacao()))
                    c6.setTextColor(Color.parseColor("#fcab65"));

                c6.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View viewi) {
                        parentRow = (TableRow) viewi.getParent();
                        textInputEditTextNumberMesa.setText(String.valueOf(mesaList.get(index).getcod_mesa()));
                        spinnerStatusMesa.setSelection(((ArrayAdapter<String>)  spinnerStatusMesa.getAdapter()).getPosition(mesaList.get(index).getSituacao()));

                       Toast.makeText(thiscontext, "Nº Mesa: "+mesaList.get(index).getcod_mesa()+"\n"+
                                                        "Situação: "+mesaList.get(index).getSituacao()+"\n",
                                Toast.LENGTH_SHORT).show();

                        indexF =  index;

                    }
                });

            }else{

                c6.setTextColor(Color.GRAY);

            }
            tableLayoutMesas.addView(tr);
        }
    }

    private void refreshMesaList(@NonNull JSONArray mesas) throws JSONException {
        mesaList.clear();

        for (int i = 0; i < mesas.length(); i++) {
            JSONObject obj = mesas.getJSONObject(i);

            mesaList.add(new Mesa(
                    obj.getInt("cod_mesa"),
                    obj.getString("status") ));
        }

        /*[Colocar Mesas na Table]*/
        atualizaMesastatus(mesaList);
    }

    public class RequestMesa extends AsyncTask<Void, Void, String> {

        private String url;
        private int requestCode;
        private HashMap<String, String> dados;
        private Context context;
        private ProgressBar progressBar;


        public  RequestMesa(String url, HashMap<String, String> dados, int requestCode, ProgressBar progressBar,
                            Context contexto) {
            this.url = url;
            this.dados= dados;
            this.requestCode = requestCode;
            this.progressBar = progressBar;
            this.context = contexto;
        }

        //quando a tarefa começou, exibir uma barra de progresso
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }



        // este método dará a resposta da solicitação
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressBar.setVisibility(GONE);
            try {
                JSONObject object = new JSONObject(s);
                if (!object.getBoolean("error")) {

                    Toast.makeText(this.context, object.getString("message"),
                            Toast.LENGTH_SHORT).show();



                    refreshMesaList(object.getJSONArray("mesas"));


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        // a operação da rede será realizada em segundo plano
        @Override
        protected String doInBackground(Void... voids) {

            RequestHandler requestHandler = new RequestHandler();
            if (requestCode == Api.CODE_POST_REQUEST)
                return requestHandler.sendPostRequest(url, dados);


            if (requestCode == Api.CODE_GET_REQUEST)
                return requestHandler.sendGetRequest(url);

            return null;


        }


    }





}