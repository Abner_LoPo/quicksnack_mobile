<?php



	
	 require_once '../includes/DbOperacoes.php';
	 

	function parametrosDisponiveis($params){
		 $available = true; 
		 $missingparams = ""; 
	 	foreach($params as $param){
	 		if(!isset($_POST[$param]) || strlen($_POST[$param])<=0){
				$available = false; 
	 			$missingparams = $missingparams . ", " . $param; 
	 		}
 		}
		 if(!$available){
			$response = array(); 
		 	$response['error'] = true; 
			$response['message'] = 'Parameters ' . substr($missingparams, 1, strlen($missingparams)) . ' missing';
			 echo json_encode($response);
			 die();
	 	}
 	}

 	 $response = array();
 	 $db = new DbOperation();
 

 	if(isset($_GET['apicall'])){

 		switch ($_GET['apicall']) {

 		
 			case 'login': 

 				parametrosDisponiveis(array('login', 'senha'));


 				$resultlg = 
 				$db->login($_POST['login'], $_POST["senha"]);

 				if(count($resultlg) > 0){

 					
 					$response['error'] = false; 
					$response['message'] = 'Seja Bem-Vindo '.explode(".",$resultlg['login'])[0].'!';
					$response['login'] = $resultlg;


 				}else{

 					$response['error'] = true; 
					$response['message'] = 'Login ou senha estão errados!';
					

 				}

 				
				
 			break;
 			

 			case 'getProdutos':

 				if(isset($_GET['categoria'])){


 						
						$response['error'] = false; 
						$response['message'] = 'Produtos Atualizados';
						$response['produtos'] = $db->getProdutos($_GET['categoria']);

 					


 				}else{

				 	 $response['error'] = true; 
					 $response['message'] = 'ID não identificado.';

 				}



 				break;

 			case 'getEstoque':

 				if(isset($_GET['descricao'])){

 					
 						$response['estoque'] = $db->getEstoque($_GET['descricao']);
 						$response['message'] = 'Produtos estoque';
 						$response['error'] = false;

 				
 				}


 				break;

 			case 'getmesas':

 			
				$response['error'] = false; 
				$response['message'] = 'Mesas Atualizadas.';
				$response['mesas'] = $db->getMesas();
 			
 				break;

 			case 'alterarMesa':
 				
 				parametrosDisponiveis(array('codmesa', 'status'));

 				$result = $db->alterarStatusMesa($_POST['codmesa'], $_POST['status']);

 				if($result){

				 	$response['error'] = false; 
				 	$response['message'] = "Mesa: '".$_POST['codmesa']."' atualizada com sucesso!";
				 	$response['mesas'] = $db->getMesas();
 			

			 	}else{
				 	$response['error'] = true; 
				 	$response['message'] = "Erro ao tentar atualizar a Mesa: '".$_POST['codmesa']."'.";
			 	}

 			break;

 			case 'getcategorias':

 				
				$response['error'] = false; 
				$response['message'] = 'Categorias Atualizadas.';
				$response['categorias'] = $db->getCategorias();

 				break;


 		}


 	}
 

 	 echo json_encode($response);



?>