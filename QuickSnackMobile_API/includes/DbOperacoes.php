<?php


class DbOperation{

	private $conn;




	function __construct(){
        require_once dirname(__FILE__) . '/DataBase.php';
        $db = new DataBase();
        $this->conn = $db->dbConnect();
	}

	function login($login, $senha){

		$sql = "call login('".$login."','".md5($senha)."')";

		$result = mysqli_query($this->conn, $sql);

		$linha = mysqli_fetch_assoc($result);

 		
			$dadosLogin = array();

		if(mysqli_num_rows($result) != 0){

			$dadosLogin["login"] = $linha['login'];
			$dadosLogin["cargo"] = $linha['cargo'];

	
		}

		return $dadosLogin;
	}




	 function getMesas(){

 		$connMs = $this->conn->prepare("SELECT * FROM  mesa");
 		$connMs->execute();
 		$connMs->bind_result($cod_mesa, $status);
 
 		$mesas = array(); 
 
 		while($connMs->fetch()){

 			$mesa  = array();
 			$mesa['cod_mesa'] = $cod_mesa; 
 			$mesa['status'] = $status; 
 
 			array_push($mesas, $mesa); 
 		}
 
 		return $mesas; 
 	}

 	function alterarStatusMesa($cod_mesa, $newStatus){

 		$connStatus = $this->conn->prepare("UPDATE mesa SET status = ? WHERE cod_mesa = ?");
 		$connStatus->bind_param("si",$newStatus, $cod_mesa);

 		if($connStatus->execute()):
 			return true;
 		endif;
 		return false;
 		
 
 		
 
 	}

 	function getProdutos($cod_categoria){

 		$connPd = $this->conn->prepare("call selectprodutos(?)");
 		$connPd->bind_param("s", $cod_categoria);

 		$connPd->execute();
 		$connPd->bind_result($descricao, $preco_venda, $max_desconto);
 
 		$produtos = array(); 
 
 		while($connPd->fetch()){

 			$produto  = array();
 			$produto['descricao'] = $descricao; 
 			$produto['preco_venda'] = $preco_venda; 
 			$produto['max_desconto'] = $max_desconto;
 
 			array_push($produtos, $produto); 
 		}
 
 		return $produtos; 
 	}



 	function getEstoque($desc){

 		if(!empty($_GET['descricao'])):
 					
 			$connEtq = $this->conn->prepare("SELECT descricao, quantidade, data_cad FROM lista_estoque WHERE descricao LIKE BINARY '".$desc."%'");


 		else:
 			$connEtq = $this->conn->prepare("SELECT descricao, quantidade, data_cad FROM lista_estoque");

 		endif;
 		



 		$connEtq->execute();
 		$connEtq->bind_result($descricao, $quantidade, $data_cad);
 
 		$estoques = array(); 
 
 		while($connEtq->fetch()){

 			$estoque  = array();
 			$estoque['descricao'] = $descricao; 
 			$estoque['quantidade'] = $quantidade; 
 			$estoque['data_cad'] = $data_cad;
 
 			array_push($estoques,$estoque); 
 		}
 
 		return $estoques; 
 	}


 	function getCategorias(){

 		$connCat = $this->conn->prepare("SELECT * FROM categorias");
 		$connCat->execute();
 		$connCat->bind_result($cod_categoria, $categoria);
 
 		$categorias = array(); 
 
 		while($connCat->fetch()){

 			$categoriaAr  = array();
 			$categoriaAr['cod_categoria'] = $cod_categoria; 
 			$categoriaAr['categoria'] = $categoria; 
 
 			array_push($categorias,$categoriaAr); 
 		}
 
 		return $categorias; 
 	}
}
?>